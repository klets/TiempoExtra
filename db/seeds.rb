@user = User.create!(email:"testo@testo.com", password:"qwerty", password_confirmation:"qwerty", first_name: "Juan", last_name:"Garcia")

puts "1 User creado"

100.times do |post|
  Post.create!(date: Date.today, rationale: "#{post} rationale content", user_id: @user.id)
end

puts "100 Posts han sido creados"
