FactoryBot.define do
  factory :post do
    date {Date.today}
    rationale {"Something"}
    user #sin esto no pasa la prueba, ya que hay una asociación
  end

  factory :second_post, class: "Post" do
    date {Date.yesterday}
    rationale {"Something else"}
    user
  end
end
