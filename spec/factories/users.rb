FactoryBot.define do
  sequence :email do |n|
    "test#{n}@mail.com"
  end

  factory :user do
    first_name {"Juan"}
    last_name {"Garcia"}
    email
    password {"qwerty"}
    password_confirmation {"qwerty"}
  end

  factory :admin_user, class: "AdminUser" do
    first_name {"admin"}
    last_name {"User"}
    email {"admin@user.com"}
    password {"qwerty"}
    password_confirmation {"qwerty"}
  end
end
