require 'rails_helper'

describe 'navigate' do
  before do
    @user = FactoryBot.create(:user) 
    #@user = User.create(email:"testo@testo.com", password:"qwerty", password_confirmation:"qwerty", first_name: "juan", last_name:"garcia")
    login_as(@user, :scope => :user)
  end
  
  describe 'index' do
    before do
      visit posts_path
    end

    it 'can be reached successfully' do
      expect(page.status_code).to eq(200)
    end

    it 'has a title of Post' do
      expect(page).to have_content(/Tiempos disponibles/)
    end

    it 'has a list of Post' do
      post1 = FactoryBot.build_stubbed(:post)
      post2 = FactoryBot.build_stubbed(:second_post)
      visit posts_path 
      expect(page).to have_content(/Usuario|Usuario/)
    end
  end

  describe 'new' do
    it 'tiene un link de página Base' do 
      visit root_path
      click_link("new_post_from_nav")
      expect(page.status_code).to eq(200)
    end
  end

  describe 'delete' do
    it 'puede ser borrado' do
      @post = FactoryBot.create(:post)
      visit posts_path
      click_link("delete_post_#{@post.id}_from_index")
      expect(page.status_code).to eq(200)
    end
  end

  describe 'creation' do
    before do
     #user = FactoryBot.create(:user)
     #login_as(user, :scope => :user)
      visit new_post_path
    end
    it 'tiene una form que puede accederse' do
      expect(page.status_code).to eq(200)
    end

    it 'pude crearse desde new form page' do
      fill_in 'post[date]', with: Date.today
      fill_in 'post[rationale]', with: "Algo en este apartado"

      click_on "Guardar"

      expect(page).to have_content("Algo en este apartado")
    end

    it 'tendrá un user asociado a este' do

      fill_in 'post[date]', with: Date.today
      fill_in 'post[rationale]', with: "Algo en este apartado por User"

      click_on "Guardar"

      expect(User.last.posts.last.rationale).to eq("Algo en este apartado por User")
    end
  end

  describe 'edit' do
    before do
      @post = FactoryBot.create(:post)
    end

    it 'puede accederse cliqueando Editar en la página de index' do

      visit posts_path
      click_link("edit_#{@post.id}")
      expect(page.status_code).to eq(200)
    end

    it 'puede ser editado' do 
      visit edit_post_path(@post)
      fill_in 'post[date]', with: Date.today
      fill_in 'post[rationale]', with: "Contenido editado"
      click_on "Guardar"
      expect(page).to have_content("Contenido editado")
    end
  end

end
